# Theme a table in the pillar Quickstart

We will repeat the exercise again for `table`.

## Laying out the table

- Open the `stylesheets/table` folder.

- `table` is in the _pillar_, so open `stylesheets/table/pillar.scss`.

- Replace with the following code:

```
table {
  border-collapse: collapse;
  display: table !important;
  text-align: left;

  td,
  th {
    padding: $personal-space;
  }

  > thead,
  > tfoot {
    td:nth-child(1) {
      min-width: 15%;
      width: 15%;
    }

    td {
      min-width: 15%;
      max-width: 70%;
    }

    th {
      border-bottom: 2px solid $god-color;
      font-size: $god-spoke;
    }
  }

  > tbody {
    border: 3px solid $god-color;

    td,
    th {
      border-bottom: 1px solid $god-color;
    }

    tr:hover > th,
    tr:hover > td {
      background: $god-blushed;
    }
  }

  > tfoot {
    td {
      padding: 10px $personal-space;
      text-align: right;
    }
  }
}
```

Ignore the detail of this. I think you'll agree it makes for a nicely formatted `table` which you can play around with later.

### What we learnt

1. The tagName must be the outer most tag in the file.

2. Use the basic settings.

3. For instance, using `$god-color` so that the `table`'s colors stay consistent with the whole theme.

4. `display: table !important;` is required to override the `display: block;` setting set by **`god`**.

5. The css can encapsulate all the tagName's children.
