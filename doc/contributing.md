# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git clone https://gitlab.com/eliosin/eve.git
cd eve
npm i|yarn
npm link @elioway/sassy-fibonacciness @elioway/god
gulp
```

## TODOS

1. Nothing? This is complete.
2. Improve documentation.
3. **eve** should make **innocent** more innocent.
